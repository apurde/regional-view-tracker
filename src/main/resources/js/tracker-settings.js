function TrackerSettings() {
	
	this.init = function() {
		AJS.$("#save-settings").click(function() {
			save();
		});
	};
	
	
	var save = function() {
		var data = {
			active : AJS.$("#active").is(":checked"),
			groupsAllowedToAdmin : AJS.$("#groupsAllowedToAdmin").val(),
			groupsAllowedToView : AJS.$("#groupsAllowedToView").val()
		};
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/regionalviewtracker/1.0/setconfiguration?spaceKey=" + AJS.$("#save-settings").attr("space-key"),
			type: "PUT",
			contentType: 'application/json',
			data: JSON.stringify(data),
			success: function(result) {
				location.reload();
			},
			error: function(request, status, error) {
				location.reload();
			}
		});
	};
}

var trackerSettings = new TrackerSettings();

AJS.toInit(function() {
	trackerSettings.init();
});