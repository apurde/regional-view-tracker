function TrackerPage() {
	
	var trackingDetails = {};
	var trackingDialog =
		'<div class="aui-tabs horizontal-tabs"> \
			<ul class="tabs-menu"> \
				<li class="menu-item active-tab"> \
					<a href="#regional-view-tracker-page-tab">$pageTab</a> \
				</li> \
				<li class="menu-item"> \
					<a href="#regional-view-tracker-space-tab">$spaceTab</a> \
				</li> \
			</ul> \
			<div class="tabs-pane active-pane" id="regional-view-tracker-page-tab"> \
				<div style="width:500px"><canvas id="regional-view-tracker-page-chart"></canvas></div> \
				<div> \
					<button id="regional-view-tracker-start" type="button" class="aui-button aui-button-link" style="display:none">$startTracking</button> \
					<button id="regional-view-tracker-stop" type="button" class="aui-button aui-button-link" style="display:none">$stopTracking</button> \
					<p id="regional-view-tracker-notTrackedHint" style="display:none">$notTrackedHint</p> \
				</div> \
			</div> \
			<div class="tabs-pane" id="regional-view-tracker-space-tab"> \
				<div style="width:500px"><canvas id="regional-view-tracker-space-chart"></canvas></div> \
			</div> \
		</div>';
	
	this.init = function() {
				
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/regionalviewtracker/1.0/" + AJS.Meta.get("page-id") + "/gettrackingdetails",
			type: "GET",
			dataType: "json",
			success: function(result) {
				trackingDetails = result;
				if(trackingDetails.spaceTrackingActive) {
					countView();
				}
				if(trackingDetails.userAllowedToView && trackingDetails.spaceTrackingActive) {
					insertChartJS();
					showTrackingIcon();
					bindTrackingDialog();
				}
			}
		});
	};
	
	
	var insertChartJS = function() {
		var script = document.createElement('script');
		script.src = "https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js";
		document.head.appendChild(script);
	};
	
	
	var showTrackingIcon = function() {
		AJS.$("ul.banner").append("<li><a href='#' id='regional-view-tracker-tracked'><span class='aui-icon aui-icon-small aui-iconfont-weblink'></a></span></li>");
	};
	
	
	var bindTrackingDialog = function() {
		
		AJS.InlineDialog(AJS.$("#regional-view-tracker-tracked"), "regional-view-tracker-details",
			function(content, trigger, showPopup) { content.css({ width: '500px' }).html(trackingDialog
			.replace("$pageTab", AJS.I18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.page.tab"))
			.replace("$spaceTab", AJS.I18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.space.tab"))
			.replace("$startTracking", AJS.I18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.startTracking"))
			.replace("$stopTracking", AJS.I18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.stopTracking"))
			.replace("$notTrackedHint", AJS.I18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.notTrackedHint")));
			showPopup(); return false; });
		
		AJS.$("#regional-view-tracker-tracked").click(function() {
			setTimeout(function() {
				AJS.tabs.setup();
				if(trackingDetails.pageTrackingActive) {
					drawChart('regional-view-tracker-page-chart', trackingDetails.pageViews);
				}
				else {
					AJS.$("#regional-view-tracker-page-chart").parent().hide();
				}
				drawChart('regional-view-tracker-space-chart', trackingDetails.spaceViews);
				if(trackingDetails.userAllowedToAdmin) {
					if(trackingDetails.pageTrackingActive) {
						AJS.$("#regional-view-tracker-stop").show();
						AJS.$("#regional-view-tracker-stop").click(function() {
							if(confirm(AJS.I18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.stop.confirmation"))) {
								stopPageTracking();
							}
						});
					}
					else {
						AJS.$("#regional-view-tracker-start").show();
						AJS.$("#regional-view-tracker-start").click(function() {
							startPageTracking();
						});
					}
				}
				else {
					if(!trackingDetails.pageTrackingActive) {
						AJS.$("#regional-view-tracker-notTrackedHint").show();
					}
				}
			}, 200);
		});
	};
	
	
	var drawChart = function(contextElementName, datasets) {
		var ctx = document.getElementById(contextElementName).getContext('2d');
		var chart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		    	labels: trackingDetails.labels,
		    	datasets: datasets,
		    },
		    options: {
		    	scales: {
					xAxes: [{
						stacked: true
					}],
					yAxes: [{
						stacked: true,
						ticks: {
							min: 0
						}
					}]
		    	}
		    }
		});
	};
	
	
	var startPageTracking = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/regionalviewtracker/1.0/" + AJS.Meta.get("page-id") + "/startpagetracking?region=" + trackingDetails.userRegionFromMail,
			type: "GET",
			dataType: "json",
			success: function(result) {
				location.reload();
			}
		});
	};
	
	
	var stopPageTracking = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/regionalviewtracker/1.0/" + AJS.Meta.get("page-id") + "/stoppagetracking",
			type: "GET",
			dataType: "json",
			success: function(result) {
				location.reload();
			}
		});
	};
	
	
	var countView = function() {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/regionalviewtracker/1.0/" + AJS.Meta.get("page-id") + "/countview?region=" + trackingDetails.userRegionFromMail,
			type: "GET",
			dataType: "json",
		});
	};
}

var trackerPage = new TrackerPage();

AJS.toInit(function() {
	trackerPage.init();
});