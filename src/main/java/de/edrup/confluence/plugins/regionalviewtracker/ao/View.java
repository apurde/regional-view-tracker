package de.edrup.confluence.plugins.regionalviewtracker.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;

@Preload
public interface View extends Entity {
	
	@NotNull
	void setTrackingId(Long trackingId);
	Long getTrackingId();
	
	@NotNull
	void setRegion(String region);
	String getRegion();
	
	@NotNull
	void setCount(Long count);
	Long getCount();
	
	@NotNull
	void setMonthId(Integer monthId);
	Integer getMonthId();
}
