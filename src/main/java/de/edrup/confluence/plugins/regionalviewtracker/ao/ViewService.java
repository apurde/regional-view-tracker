package de.edrup.confluence.plugins.regionalviewtracker.ao;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import net.java.ao.DBParam;
import net.java.ao.Query;

@Named
public class ViewService {
	
	private final ActiveObjects ao;
	
	
	@Inject
	public ViewService(@ComponentImport ActiveObjects ao) {
		this.ao = ao;
	}
	
	
	public boolean isTracked(Long trackingId) {
		return ao.find(View.class, Query.select().where("TRACKING_ID = ?", trackingId)).length > 0;
	}
	
	
	public View setIncreaseView(Long trackingId, String region, Integer monthId, Long count) {
		if(!hasViewsInRegion(trackingId, region, monthId)) {
			return initViewForRegion(trackingId, region, monthId, (count == null) ? 1L : count);
		}
		else {
			View view = getViewsForRegion(trackingId, region, monthId);
			if(view != null) {
				view.setCount((count == null) ? view.getCount() + 1 : count);
				view.save();
			}
			return view;
		}
	}
	
	
	public void removeTrackingId(Long trackingId) {
		ao.delete(ao.find(View.class, Query.select().where("TRACKING_ID = ?", trackingId)));
	}
		
	
	public List<String> getRegions(Long trackingId) {
		return Arrays.asList(ao.find(View.class, Query.select().where("TRACKING_ID = ?", trackingId))).stream().map(x -> x.getRegion()).distinct().collect(Collectors.toList());
	}
	
	
	public Long getViewCountForRegion(Long trackingId, String region, Integer monthId) {
		View view = getViewsForRegion(trackingId, region, monthId);
		return (view == null) ? 0 : view.getCount();
	}
	
		
	private View getViewsForRegion(Long trackingId, String region, Integer monthId) {
		View view[] = ao.find(View.class, Query.select().where("TRACKING_ID = ? AND REGION = ? AND MONTH_ID = ?", trackingId, region, monthId));
		if(view.length > 0) {
			return view[0];
		}
		else {
			return null;
		}
	}
	
	
	private boolean hasViewsInRegion(Long trackingId, String region, Integer monthId) {
		return (getViewsForRegion(trackingId, region, monthId) != null);
	}
		
	
	private View initViewForRegion(Long trackingId, String region, Integer monthId, Long count) {
		 View view = ao.create(View.class, new DBParam("TRACKING_ID", trackingId), new DBParam("REGION", region),
			new DBParam("COUNT", count), new DBParam("MONTH_ID", monthId));
		 view.save();
		 return view;
	}
}
