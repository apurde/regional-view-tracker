package de.edrup.confluence.plugins.regionalviewtracker.config;

import javax.inject.Inject;

import com.atlassian.confluence.spaces.actions.SpaceAdminAction;
import com.atlassian.json.jsonorg.JSONObject;

import de.edrup.confluence.plugins.regionalviewtracker.util.TrackerHelper;

public class TrackerSpaceConfigurationAction extends SpaceAdminAction {

	private static final long serialVersionUID = 1L;
	
	private final TrackerHelper trackerHelper;
	
	
	@Inject
	public TrackerSpaceConfigurationAction(TrackerHelper trackerHelper) {
		this.trackerHelper = trackerHelper;
	}
	
	
	@Override
	public String execute() throws Exception {
		super.execute();
		return SUCCESS;
	}
	
	
	public JSONObject getConfiguration() {
		return new JSONObject(trackerHelper.getConfiguration(this.getSpace().getKey()));
	}
}
