package de.edrup.confluence.plugins.regionalviewtracker.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.confluence.compat.api.service.accessmode.ReadOnlyAccessBlocked;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.regionalviewtracker.util.TrackerHelper;

@Path("/")
public class TrackerRest {
	
	private final TrackerHelper trackerHelper;
	private final PermissionManager permissionMan;
	private final SpaceManager spaceMan;
	private final PageManager pageMan;
	
	
	@Inject
	public TrackerRest(TrackerHelper trackerHelper, @ComponentImport PermissionManager permissionMan,
		@ComponentImport SpaceManager spaceMan, @ComponentImport PageManager pageMan) {
		this.trackerHelper = trackerHelper;
		this.permissionMan = permissionMan;
		this.spaceMan = spaceMan;
		this.pageMan = pageMan;
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/getconfiguration")
	public Response getConfiguration(@QueryParam("spaceKey") String spaceKey) {
		return Response.ok(trackerHelper.getConfiguration(spaceKey)).cacheControl(getNoStoreNoCacheControl()).build();
	}

	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/setconfiguration")
	public Response setConfiguration(@QueryParam("spaceKey") String spaceKey, String configuration) {
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		if(user == null) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		if(spaceMan.getSpace(spaceKey) == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(!spaceMan.getSpaceAdmins(spaceMan.getSpace(spaceKey)).contains(user) && !permissionMan.isConfluenceAdministrator(user)) {
			return Response.status(Status.UNAUTHORIZED).build();						
		}
		trackerHelper.setConfiguration(spaceKey, configuration);
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("{pageId}/countview")
	@ReadOnlyAccessBlocked
	public Response countView(@PathParam("pageId") String pageId, @QueryParam("region") String region) {
		if(pageMan.getPage(Long.parseLong(pageId)) != null) {
			trackerHelper.countView(Long.parseLong(pageId), region);
		}
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("{pageId}/startpagetracking")
	@ReadOnlyAccessBlocked
	public Response startPageTracking(@PathParam("pageId") String pageId, @QueryParam("region") String region) {
		trackerHelper.startPageTracking(Long.parseLong(pageId), region);
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("{pageId}/stoppagetracking")
	@ReadOnlyAccessBlocked
	public Response stopPageTracking(@PathParam("pageId") String pageId) {
		trackerHelper.stopPageTracking(Long.parseLong(pageId));
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("{pageId}/setpageviewcount")
	@ReadOnlyAccessBlocked
	public Response setPageViewCount(@PathParam("pageId") String pageId, @QueryParam("region") String region, @QueryParam("monthId") String monthId,  @QueryParam("count") String count) {
		trackerHelper.setPageViewCount(Long.parseLong(pageId), region, Integer.parseInt(monthId), Long.parseLong(count));
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("{spaceKey}/setspaceviewcount")
	@ReadOnlyAccessBlocked
	public Response setSpaceViewCount(@PathParam("spaceKey") String spaceKey, @QueryParam("region") String region, @QueryParam("monthId") String monthId,  @QueryParam("count") String count) {
		trackerHelper.setSpaceViewCount(spaceKey, region, Integer.parseInt(monthId), Long.parseLong(count));
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("{pageId}/gettrackingdetails")
	public Response getTrackingDetails(@PathParam("pageId") String pageId) {
		return Response.ok(trackerHelper.getTrackingDetailsForPage(Long.parseLong(pageId)).toString()).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	private CacheControl getNoStoreNoCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setNoStore(true);
		cc.setMustRevalidate(true);
		return cc;
	}
}
