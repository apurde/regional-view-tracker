package de.edrup.confluence.plugins.regionalviewtracker.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

@Preload
public interface Configuration extends Entity {

	@NotNull
	void setSpaceKey(String spaceKey);
	String getSpaceKey();
	
	@NotNull
	@StringLength(value=StringLength.UNLIMITED)
	void setConfiguration(String configuration);
	String getConfiguration();	
}