package de.edrup.confluence.plugins.regionalviewtracker.ao;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.regionalviewtracker.ao.Configuration;
import net.java.ao.DBParam;
import net.java.ao.Query;

@Named
public class ConfigurationService {
	
	private final ActiveObjects ao;
	
	
	@Inject
	public ConfigurationService(@ComponentImport ActiveObjects ao) {
		this.ao = ao;
	}

	
	public Configuration setConfiguration(String spaceKey, String configuration) {
		Configuration checkoutConfiguration = getConfiguration(spaceKey);
		if(checkoutConfiguration == null) {
			checkoutConfiguration = ao.create(Configuration.class, new DBParam("SPACE_KEY", spaceKey), new DBParam("CONFIGURATION", configuration));
		}
		else {
			checkoutConfiguration.setConfiguration(configuration);
		}
		checkoutConfiguration.save();
		return checkoutConfiguration;			
	}
	
	
	public Configuration getConfiguration(String spaceKey) {
		Configuration checkoutConfiguration[] = ao.find(Configuration.class, Query.select().where("SPACE_KEY = ?", spaceKey));
		if(checkoutConfiguration.length > 0) {
			return checkoutConfiguration[0];
		}
		else {
			return null;
		}
	}
	
	
	public boolean hasConfiguration(String spaceKey) {
		return ao.find(Configuration.class, Query.select().where("SPACE_KEY = ?", spaceKey)).length > 0;
	}
}
