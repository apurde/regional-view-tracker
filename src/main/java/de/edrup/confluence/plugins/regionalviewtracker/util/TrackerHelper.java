package de.edrup.confluence.plugins.regionalviewtracker.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.json.jsonorg.JSONArray;
import com.atlassian.json.jsonorg.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

import de.edrup.confluence.plugins.regionalviewtracker.ao.ConfigurationService;
import de.edrup.confluence.plugins.regionalviewtracker.ao.ViewService;

@Named
public class TrackerHelper {
	
	private final ConfigurationService configurationService;
	private final ViewService viewService;
	private final PageManager pageMan;
	private final UserAccessor userAcc;
	private final SpaceManager spaceMan;
	private final I18nResolver i18n;
	
	private final String[] mpn65 = {
		"ff0029", "377eb8", "66a61e", "984ea3", "00d2d5", "ff7f00", "af8d00",
	    "7f80cd", "b3e900", "c42e60", "a65628", "f781bf", "8dd3c7", "bebada",
	    "fb8072", "80b1d3", "fdb462", "fccde5", "bc80bd", "ffed6f", "c4eaff",
	    "cf8c00", "1b9e77", "d95f02", "e7298a", "e6ab02", "a6761d", "0097ff",
	    "00d067", "000000", "252525", "525252", "737373", "969696", "bdbdbd",
	    "f43600", "4ba93b", "5779bb", "927acc", "97ee3f", "bf3947", "9f5b00",
	    "f48758", "8caed6", "f2b94f", "eff26e", "e43872", "d9b100", "9d7a00",
	    "698cff", "d9d9d9", "00d27e", "d06800", "009f82", "c49200", "cbe8ff",
	    "fecddf", "c27eb6", "8cd2ce", "c4b8d9", "f883b0", "a49100", "f48800",
	    "27d0df", "a04a9b"};
	
	
	@Inject
	public TrackerHelper(ConfigurationService configurationService, ViewService viewService,
		@ComponentImport PageManager pageMan, @ComponentImport UserAccessor userAcc, @ComponentImport SpaceManager spaceMan,
		@ComponentImport I18nResolver i18n) {
		this.configurationService = configurationService;
		this.viewService = viewService;
		this.pageMan = pageMan;
		this.userAcc = userAcc;
		this.spaceMan = spaceMan;
		this.i18n = i18n;
	}
	
	
	public void countView(Long pageId, String region) {
		if(isTrackingActiveForSpace(pageId)) {
			if(viewService.isTracked(pageId)) {
				viewService.setIncreaseView(pageId, region, getCurrentMonthId(), null);
			}
			viewService.setIncreaseView(getSpaceId(pageId), region, getCurrentMonthId(), null);
		}
	}
	
	
	public void setPageViewCount(Long pageId, String region, Integer monthId, Long count) {
		if(isUserAllowed(pageMan.getAbstractPage(pageId).getSpaceKey(), "groupsAllowedToAdmin")) {
			viewService.setIncreaseView(pageId, region, monthId, count);
		}
	}
	
	
	public void setSpaceViewCount(String spaceKey, String region, Integer monthId, Long count) {
		if(isUserAllowed(spaceKey, "groupsAllowedToAdmin")) {
			viewService.setIncreaseView(getSpaceId(spaceKey), region, monthId, count);
		}
	}
	
	
	public void startPageTracking(Long pageId, String region) {
		if(isTrackingActiveForSpace(pageId) && isUserAllowed(pageMan.getAbstractPage(pageId).getSpaceKey(), "groupsAllowedToAdmin") && !viewService.isTracked(pageId)) {
			viewService.setIncreaseView(pageId, region, getCurrentMonthId(), 0L);
		}		
	}
	
	
	public void stopPageTracking(Long pageId) {
		if(isUserAllowed(pageMan.getAbstractPage(pageId).getSpaceKey(), "groupsAllowedToAdmin")) {
			viewService.removeTrackingId(pageId);
		}
	}
	
	
	public JSONObject getTrackingDetailsForPage(Long pageId) {
		
		JSONObject details = new JSONObject();
		
		Long spaceId = getSpaceId(pageId);
		String spaceKey = pageMan.getAbstractPage(pageId).getSpaceKey();
		Integer currentMonthId = getCurrentMonthId();
		JSONObject configuration = getConfigurationJSON(spaceKey);
		
		boolean userAllowedToView = isUserAllowed(spaceKey, "groupsAllowedToView");
		details.put("userAllowedToView", userAllowedToView);
		details.put("userAllowedToAdmin", isUserAllowed(spaceKey, "groupsAllowedToAdmin"));
	
		details.put("spaceTrackingActive", configuration.getBoolean("active"));
		
		details.put("userRegionFromMail", userRegionFromMail());
		
		ArrayList<String> rotatedMonths = new ArrayList<String>();
		String[] monthNames = i18n.getText("de.edrup.confluence.plugins.regional-view-tracker.ui.months").split(","); 
		for(int m = 11; m >= 0; m--) {
			rotatedMonths.add(monthNames[(currentMonthId - m) % 12]);
		}
		details.put("labels", rotatedMonths);
		
		if(configuration.getBoolean("active") && userAllowedToView) {
			
			List<String> regionsSpace = viewService.getRegions(spaceId);
			JSONArray spaceViews = new JSONArray();
			int colorCounter = 0;
			for(String region : regionsSpace) {
				JSONObject spaceView = new JSONObject();
				spaceView.put("label", region);
				ArrayList<Long> views = new ArrayList<Long>();
				for(int m = 11; m >= 0; m--) {
					views.add(viewService.getViewCountForRegion(spaceId, region, currentMonthId - m));
				}
				spaceView.put("data", views);
				spaceView.put("backgroundColor", "#" + mpn65[colorCounter % 65]);
				colorCounter++;
				spaceViews.put(spaceView);
			}
			details.put("spaceViews", spaceViews);
			
			details.put("pageTrackingActive", viewService.isTracked(pageId));
			List<String> regionsPage = viewService.getRegions(pageId);
			JSONArray pageViews = new JSONArray();
			colorCounter = 0;
			for(String region : regionsPage) {
				JSONObject pageView = new JSONObject();
				pageView.put("label", region);
				ArrayList<Long> views = new ArrayList<Long>();
				for(int m = 11; m >= 0; m--) {
					views.add(viewService.getViewCountForRegion(pageId, region, currentMonthId - m));
				}
				pageView.put("data", views);
				pageView.put("backgroundColor", "#" + mpn65[colorCounter % 65]);
				colorCounter++;
				pageViews.put(pageView);
			}
			details.put("pageViews", pageViews);
		}
		
		return details;
	}
	
	
	private Long getSpaceId(Long pageId) {
		return pageMan.getPage(pageId).getSpace().getId();
	}
	
	
	private Long getSpaceId(String spaceKey) {
		return spaceMan.getSpace(spaceKey).getId();
	}
	
	
	private boolean isUserAllowed(String spaceKey, String configurationKey) {
		ArrayList<String> groupNames = new ArrayList<String>();
		groupNames.addAll(userAcc.getGroupNames(AuthenticatedUserThreadLocal.get()));
		if(spaceMan.getSpaceAdmins(spaceMan.getSpace(spaceKey)).contains(AuthenticatedUserThreadLocal.get())) {
			groupNames.add("space-admins");
		}
		List<String> allowedGroups = Arrays.asList(getConfigurationJSON(spaceKey).getString(configurationKey).split(" *, *"));
		return !Collections.disjoint(groupNames, allowedGroups);
	}
	
	
	private String userRegionFromMail() {
		if(AuthenticatedUserThreadLocal.get().getEmail() != null) {
			return AuthenticatedUserThreadLocal.get().getEmail().replaceAll(".*\\.", "");
		}
		else {
			return "xx";
		}
	}
	
	
	private boolean isTrackingActiveForSpace(Long pageId) {
		if(pageMan.getAbstractPage(pageId) == null) {
			return false;
		}
		return getConfigurationJSON(pageMan.getAbstractPage(pageId).getSpaceKey()).getBoolean("active");
	}
	
	
	private Integer getCurrentMonthId() {
		return (Calendar.getInstance().get(Calendar.YEAR) - 2000) * 12 + Calendar.getInstance().get(Calendar.MONTH);
	}
	
	
	public String getConfiguration(String spaceKey) {
		return getConfigurationJSON(spaceKey).toString();
	}
	
	
	private JSONObject getConfigurationJSON(String spaceKey) {
		
		JSONObject configuration = configurationService.hasConfiguration(spaceKey) ? new JSONObject(configurationService.getConfiguration(spaceKey).getConfiguration()) : new JSONObject();
		
		putIfAbsent(configuration, "active", false);
		putIfAbsent(configuration, "groupsAllowedToAdmin", "space-admins");
		putIfAbsent(configuration, "groupsAllowedToView", "confluence-users");
		
		return configuration;
	}
	
	
	private void putIfAbsent(JSONObject object, String key, boolean value) {
		if(!object.has(key)) {
			object.put(key, value);
		}
	}

	
	private void putIfAbsent(JSONObject object, String key, String value) {
		if(!object.has(key)) {
			object.put(key, value);
		}
	}

	
	public void setConfiguration(String spaceKey, String configuration) {
		configurationService.setConfiguration(spaceKey, configuration);
	}
}
